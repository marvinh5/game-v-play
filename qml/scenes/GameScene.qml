import VPlay 2.0
import QtQuick 2.0
import "../common"
import QtWebKit 3.0

SceneBase {
    id:gameScene
    // the filename of the current level gets stored here, it is used for loading the
    property string activeLevelFileName
    // the currently loaded level gets stored here
    property variant activeLevel
    // score
    property int score: 0
    // countdown shown at level start
    property int countdown: 0
    // flag indicating if game is running
    property bool gameRunning: countdown == 0

    // set the name of the current level, this will cause the Loader to load the corresponding level
    function setLevel(fileName) {
        activeLevelFileName = fileName
    }



    // background
    Rectangle {
        id:backgroundGrass
        anchors.fill: parent.gameWindowAnchorItem
        color: "#dd94da"
        Image{
            source: "../../assets/img/grass.png"
        }
        MouseArea{
            id:mouseAreaBackground
            width: backgroundGrass.width
            height:backgroundGrass.height
            onClicked: {
                cannonHead.transformOrigin = 4

                entityManager.entityContainer = gameScene

                var newEntityProperties = {
                                    x: cannonHead.x - 5,
                                    y: cannonHead.y + cannonHead.height/2,
                                    transformOrigin: 4,
                                    rotation:cannonHead.rotation,
                                    z:3
                                }

                entityManager.createEntityFromUrlWithProperties((Qt.resolvedUrl("../common/CannonBall.qml")),newEntityProperties)

                timerRealingHead.start()
            }
        }
    }

    EntityBase{
        id:cannonBall
        entityId: "movingObject2"
        entityType: "movingObject"
        height: 10
        width: 10
        opacity: 0

        Rectangle{
            color:"red"
            height: 10
            width: 10
        }
    }

    property int angleHead:0
    Timer{
        id:timerRealingHead
        interval: 1
        repeat: true
        running: false
        onTriggered: {

            var speed = 5

            var angle = getAngleOfCannonHead(cannonHead, mouseAreaBackground)

            var angleHeadBase360 = Math.abs(((angleHead) % 360))

            console.log("angle:", angle, "angleHead:", angleHeadBase360)

            if(angle !== angleHeadBase360)
            {
                if(0 <= angleHeadBase360 && angleHeadBase360 < 180 && 0 <= angle && angle <= 180
                        || 180 <= angleHeadBase360 && angleHeadBase360 <= 360 && 180 <= angle && angle <= 360)
                {
                    if(angle > angleHeadBase360)
                        angleHead += angleHeadBase360 + speed > angle ? Math.abs(angleHeadBase360-angle):speed
                    else
                        angleHead -= angleHeadBase360 - speed < angle ? Math.abs(angleHeadBase360-angle):speed
                }
                else
                {
                    if(angle > angleHeadBase360)
                    {
                        var otherSide =  (angleHeadBase360 + 180) % 360

                        if(angle < otherSide)
                            angleHead += speed

                        else if(angle > otherSide )

                            angleHead = angleHead - speed < 0 ? 360:angleHead - speed

                    }
                    else{
                        var otherSide2 = (angleHeadBase360 + 180) % 360

                        if(angle > otherSide2)
                            angleHead = angleHead - speed < 0 ? 360:angleHead - speed
                        else if(angle < otherSide2)
                            angleHead += speed

                    }
                }
                cannonHead.rotation = angleHead
            }else{
                running= false
            }
        }
    }

    // back button to leave scene
    MenuButton {
        text: "Back to menu"
        // anchor the button to the gameWindowAnchorItem to be on the edge of the screen on any device
        anchors.right: gameScene.gameWindowAnchorItem.right
        anchors.rightMargin: 10
        anchors.top: gameScene.gameWindowAnchorItem.top
        anchors.topMargin: 10
        onClicked: {
            backButtonPressed()
            activeLevel = undefined
            activeLevelFileName = ""
        }
    }


    // name of the current level
    Text {
        anchors.left: gameScene.gameWindowAnchorItem.left
        anchors.leftMargin: 10
        anchors.top: gameScene.gameWindowAnchorItem.top
        anchors.topMargin: 10
        color: "white"
        font.pixelSize: 20
        text: activeLevel !== undefined ? activeLevel.levelName : ""
    }

    // load levels at runtime
   /* Loader {
        id: loader
        source: activeLevelFileName != "" ? "../levels/" + activeLevelFileName : ""
        /*onLoaded: {
            // reset the score
            score = 0
            // since we did not define a width and height in the level item itself, we are doing it here
            item.width = gameScene.width
            item.height = gameScene.height
            // store the loaded level as activeLevel for easier access
            activeLevel = item
            // restarts the countdown
            countdown = 3
        }
    }
*/
    // we connect the gameScene to the loaded level
    Connections {
        // only connect if a level is loaded, to prevent errors
        target: activeLevel !== undefined ? activeLevel : null
        // increase the score when the rectangle is clicked
        onRectanglePressed: {
            // only increase score when game is running
            if(gameRunning) {
                score++
            }
        }
    }

   function getAngleOfCannonHead(_cannonHead, mouse)
   {
       var xdiff =  (_cannonHead.x + _cannonHead.width / 2 ) - mouse.mouseX

       var ydiff =  (_cannonHead.y + _cannonHead.height / 2)  - mouse.mouseY

       var result = Math.atan2(ydiff, xdiff) * (180/Math.PI)

       //console.log(xdiff, ydiff, result)

       result = result < 0 ? result + 360:result

       return parseInt(result)
   }



    Canon{
        id: player

        anchors.horizontalCenter: parent.horizontalCenter

        anchors.bottom: gameScene.gameWindowAnchorItem.bottom

        MouseArea{
            id:mouseArea
            height:player.height
            width: player.width
            onClicked: {
                explosion.jumpTo("explode")

            }
        }

        z:2

    }


    CannonHead{
        z:2
        id:cannonHead
        anchors.horizontalCenter: player.horizontalCenter
        anchors.verticalCenter: player.verticalCenter
    }

    Rectangle{
        id:littleRectangle
        color:"white"
        width: 10
        height: 10
        x:parent.width / 2
        y:parent.height / 2
        opacity: 0

    }




    Explosion{
        id:explosion
        x:player.x + (player.width - explosion.width) / 2
        y:player.y - explosion.height / 1.5
        z:3
    }

    // name of the current level
    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: gameScene.gameWindowAnchorItem.top
        anchors.topMargin: 30
        color: "white"
        font.pixelSize: 40
        text: score
    }

    // text displaying either the countdown or "tap!"

    // if the countdown is greater than 0, this timer is triggered every second, decreasing the countdown (until it hits 0 again)
   /* Timer {
        repeat: true
        running: true
        interval: 50
        onTriggered: {
            cannonBall.y -= 10
            if(cannonBall.collisioned){
                running = false
            }
        }
    }
*/
}

