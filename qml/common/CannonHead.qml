import QtQuick 2.0
import VPlay 2.0

EntityBase{
    id:player
    entityType: "player"
    width:cannon.width
    height: cannon.height

    SpriteSequenceVPlay{
        id: cannon
        anchors.centerIn: parent

        SpriteVPlay{
            frameCount: 1
            frameRate: 10
            frameWidth: 87
            frameHeight: 69
            source: "../../assets/img/cannonLauncher.png"
        }
    }
}
