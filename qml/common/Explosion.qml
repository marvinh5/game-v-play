import QtQuick 2.0
import VPlay 2.0



    SpriteSequenceVPlay{

        id: sprite

        defaultSource:"../../assets/img/explosion2.png"

        SpriteVPlay{
            name:"nothing"
            frameCount: 25
            frameRate: 20
            frameWidth: 40
            opacity:1
            frameHeight: 40
            startFrameColumn: 5
            startFrameRow: 5
            source: "../../assets/img/explosion2.png"
        }

        SpriteVPlay{
            name:"explode"
            frameCount: 25
            frameRate: 10
            frameWidth: 40
            opacity:1
            frameHeight: 40
            source: "../../assets/img/explosion2.png"
            to:{"nothing":1}
        }


    }

