import QtQuick 2.0
import VPlay 2.0


EntityBase{

        id:cannonBallEntity

        entityId: cannonballId

        Rectangle{
            id:rectangleBean
            color:"orange"
            width:10
            height: 10
        }

        BoxCollider{
            anchors.fill: rectangleBean
            fixture.onBeginContact:{
                console.log("contact")
                rectangleBean.color = "red"
                timer.stop()
                cannonBallEntity.opacity = 0
            }
        }
        Loader{
            onLoaded: {
                timer.start()
            }
        }

        Timer{
            id:timer
            repeat:true
            interval: 1
            running:true
            onTriggered: {
                cannonBallEntity.x--
                cannonBallEntity.x++
            }
        }

       /* BoxCollider{
            anchors.fill: explodeSequence
            fixture.onBeginContact: {
                explodeSequence.jumpTo("explode")
                collisioned = true
            }
        }

        SpriteSequenceVPlay{
            id:explodeSequence
            defaultSource: "../../assets/img/cannonball.png"

            SpriteVPlay{
                name:"nothing"
                frameCount: 1
                frameRate: 20
                frameWidth: 10
                opacity:1
                frameHeight: 10
                startFrameColumn: 1
                startFrameRow: 1
                source: "../../assets/img/cannonball.png"
            }

            SpriteVPlay{
                name:"explode"
                frameCount: 3
                frameRate: 10
                frameWidth: 10
                opacity:1
                frameHeight: 10
                source: "../../assets/img/explosion2.png"
                to:{"nothing":1}
            }
        }
*/
}




